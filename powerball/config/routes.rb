Rails.application.routes.draw do

   get "/", :to => redirect("/default.html")

  #Configuration Swagger
    mount GrapeSwaggerRails::Engine => '/swagger'
    
  #API
    mount API => '/'

end
