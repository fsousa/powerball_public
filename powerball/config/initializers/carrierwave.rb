# CarrierWave.configure do |config|
#   config.storage    = :aws
#   config.aws_bucket = ENV["AWS_BUCKET_NAME"] || "powerball-api-data-s3"
#   config.aws_acl    = 'public-read'
#   config.fog_directory  = "powerball-api-data-s3"

#   # Optionally define an asset host for configurations that are fronted by a
#   # content host, such as CloudFront.
#   # config.asset_host = 'http://example.com'

#   # The maximum period for authenticated_urls is only 7 days.
#   config.aws_authenticated_url_expiration = 60 * 60 * 24 * 7

#   # Set custom options such as cache control to leverage browser caching
#   config.aws_attributes = {
#     expires: 1.week.from_now.httpdate,
#     cache_control: 'max-age=604800'
#   }

CarrierWave.configure do |config|
  config.fog_credentials = {
      :provider               => ENV['CARRIER_WAVE_PROVIDER'],
      :aws_access_key_id      => ENV['CARRIER_WAVE_ACCESS_KEY'],
      :aws_secret_access_key  => ENV['CARRIER_WAVE_SECRET_ACCESS_KEY'],
      :region                 => ENV['CARRIER_WAVE_REGION']
  }
  config.fog_directory  = ENV['CARRIER_WAVE_DIR']
end