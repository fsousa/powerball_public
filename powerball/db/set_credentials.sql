-- Create the user account and grant it all privileges on the databases.
grant all PRIVILEGES on powerball_development.* to powerball@localhost identified by 'powerball!';
grant all PRIVILEGES on powerball_test.* to powerball@localhost identified by 'powerball!';
grant all PRIVILEGES on powerball.* to powerball@localhost identified by 'powerball!';