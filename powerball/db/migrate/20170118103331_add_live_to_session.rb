class AddLiveToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :live, :boolean, default: false
  end
end
