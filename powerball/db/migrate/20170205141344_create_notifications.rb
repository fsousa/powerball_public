class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.string :notification_type
      t.string :notification_content
      t.references :session, index: true, foreign_key: true
      t.references :viewer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
