class AddSecretToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :secret, :string
  end
end
