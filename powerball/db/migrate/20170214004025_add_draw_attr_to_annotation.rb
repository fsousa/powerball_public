class AddDrawAttrToAnnotation < ActiveRecord::Migration
  def change
    add_column :annotations, :x_offset, :string
    add_column :annotations, :y_offset, :string
    add_column :annotations, :zoom, :string
  end
end
