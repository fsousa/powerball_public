class AddPresenterIdToSessions < ActiveRecord::Migration
  def change
    add_reference :sessions, :presenter, index: true, foreign_key: true
  end
end
