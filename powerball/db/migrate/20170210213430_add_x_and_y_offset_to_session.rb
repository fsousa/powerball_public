class AddXAndYOffsetToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :x_offset, :string
    add_column :sessions, :y_offset, :string
  end
end
