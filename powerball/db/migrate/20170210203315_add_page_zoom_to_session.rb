class AddPageZoomToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :zoom, :string
  end
end
