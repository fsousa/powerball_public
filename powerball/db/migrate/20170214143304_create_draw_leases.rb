class CreateDrawLeases < ActiveRecord::Migration
  def change
    create_table :draw_leases do |t|
      t.string :draw_token
      t.references :session, index: true, foreign_key: true
      t.references :viewer, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
