class AddIsOpenToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :is_open, :boolean, default: false
  end
end
