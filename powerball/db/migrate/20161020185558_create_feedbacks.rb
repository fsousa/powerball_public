class CreateFeedbacks < ActiveRecord::Migration
  def change
    create_table :feedbacks do |t|
      t.references :viewer, index: true, foreign_key: true
      t.references :session, index: true, foreign_key: true
      t.references :resource, index: true, foreign_key: true
      t.string :description

      t.timestamps null: false
    end
  end
end
