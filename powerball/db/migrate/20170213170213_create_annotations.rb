class CreateAnnotations < ActiveRecord::Migration
  def change
    create_table :annotations do |t|
      t.string :note
      t.string :draw
      t.references :resource, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
