class AddPresenterIdAndSessionIdToResource < ActiveRecord::Migration
  def change
  	add_reference :resources, :presenter, index: true, foreign_key: true
  	add_reference :resources, :session, index: true, foreign_key: true

  end
end
