class CreateViewers < ActiveRecord::Migration
  def change
    create_table :viewers do |t|
      t.string :name
      t.references :session, index: true, foreign_key: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
