class RemoveOffsetFromSession < ActiveRecord::Migration
  def change
  	remove_column :sessions, :x_offset
    remove_column :sessions, :y_offset
  end
end
