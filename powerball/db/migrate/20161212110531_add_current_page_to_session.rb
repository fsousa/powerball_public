class AddCurrentPageToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :current_page, :integer, default: 0
  end
end
