class AddLocationToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :lat, :string
    add_column :sessions, :lng, :string
  end
end
