class AddIndexToSession < ActiveRecord::Migration
  def change
  	add_index :sessions, :key, :unique => true
  end
end
