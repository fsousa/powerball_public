class AddReadyToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :ready, :boolean, default: false
  end
end
