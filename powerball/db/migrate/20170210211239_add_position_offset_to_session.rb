class AddPositionOffsetToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :position_offset, :string
  end
end
