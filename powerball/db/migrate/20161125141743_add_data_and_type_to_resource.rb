class AddDataAndTypeToResource < ActiveRecord::Migration
  def change
    add_column :resources, :data, :string
    add_column :resources, :type, :string
  end
end
