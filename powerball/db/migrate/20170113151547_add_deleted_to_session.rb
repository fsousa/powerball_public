class AddDeletedToSession < ActiveRecord::Migration
  def change
    add_column :sessions, :deleted, :boolean, default: false
  end
end
