# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

if User.find_by_username("felipe").nil?
	User.create(:username => "felipe", :password => "f3l1p3", :first_name => "Felipe", :last_name => "Silva", :email => "felipe@quimbik.com")
end

if User.find_by_username("User1").nil?
	User.create(:username => "User1", :password => "user123456", :first_name => "User1", :last_name => "Test", :email => "test1@test.com")
end

if User.find_by_username("User2").nil?
	User.create(:username => "User2", :password => "user123456", :first_name => "User2", :last_name => "Test", :email => "test2@test.com")
end

if User.find_by_username("User3").nil?
	User.create(:username => "User3", :password => "user123456", :first_name => "User3", :last_name => "Test", :email => "test3@test.com")
end

if User.find_by_username("User4").nil?
	User.create(:username => "User4", :password => "user123456", :first_name => "User4", :last_name => "Test", :email => "test4@test.com")
end

if User.find_by_username("User5").nil?
	User.create(:username => "User5", :password => "user123456", :first_name => "User5", :last_name => "Test", :email => "test5@test.com")
end

if User.find_by_username("User6").nil?
	User.create(:username => "User6", :password => "user123456", :first_name => "User6", :last_name => "Test", :email => "test6@test.com")
end
