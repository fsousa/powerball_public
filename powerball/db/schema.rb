# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170214143304) do

  create_table "annotations", force: :cascade do |t|
    t.string   "note",        limit: 255
    t.string   "draw",        limit: 255
    t.integer  "resource_id", limit: 4
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "page",        limit: 4
    t.string   "x_offset",    limit: 255
    t.string   "y_offset",    limit: 255
    t.string   "zoom",        limit: 255
  end

  add_index "annotations", ["resource_id"], name: "index_annotations_on_resource_id", using: :btree

  create_table "api_keys", force: :cascade do |t|
    t.string   "access_token", limit: 255
    t.datetime "expires_at"
    t.integer  "user_id",      limit: 4
    t.boolean  "active"
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  add_index "api_keys", ["access_token"], name: "index_api_keys_on_access_token", unique: true, using: :btree
  add_index "api_keys", ["user_id"], name: "index_api_keys_on_user_id", using: :btree

  create_table "draw_leases", force: :cascade do |t|
    t.string   "draw_token", limit: 255
    t.integer  "session_id", limit: 4
    t.integer  "viewer_id",  limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "draw_leases", ["session_id"], name: "index_draw_leases_on_session_id", using: :btree
  add_index "draw_leases", ["viewer_id"], name: "index_draw_leases_on_viewer_id", using: :btree

  create_table "feedbacks", force: :cascade do |t|
    t.integer  "viewer_id",   limit: 4
    t.integer  "session_id",  limit: 4
    t.integer  "resource_id", limit: 4
    t.string   "description", limit: 255
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "feedbacks", ["resource_id"], name: "index_feedbacks_on_resource_id", using: :btree
  add_index "feedbacks", ["session_id"], name: "index_feedbacks_on_session_id", using: :btree
  add_index "feedbacks", ["viewer_id"], name: "index_feedbacks_on_viewer_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.string   "notification_type",    limit: 255
    t.string   "notification_content", limit: 255
    t.integer  "session_id",           limit: 4
    t.integer  "viewer_id",            limit: 4
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
  end

  add_index "notifications", ["session_id"], name: "index_notifications_on_session_id", using: :btree
  add_index "notifications", ["viewer_id"], name: "index_notifications_on_viewer_id", using: :btree

  create_table "presenters", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "presenters", ["user_id"], name: "index_presenters_on_user_id", using: :btree

  create_table "resources", force: :cascade do |t|
    t.string   "name",          limit: 255
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "presenter_id",  limit: 4
    t.integer  "session_id",    limit: 4
    t.string   "data",          limit: 255
    t.string   "resource_type", limit: 255
    t.integer  "position",      limit: 4
  end

  add_index "resources", ["presenter_id"], name: "index_resources_on_presenter_id", using: :btree
  add_index "resources", ["session_id"], name: "index_resources_on_session_id", using: :btree

  create_table "sessions", force: :cascade do |t|
    t.string   "name",            limit: 255
    t.string   "key",             limit: 255
    t.datetime "created_at",                                  null: false
    t.datetime "updated_at",                                  null: false
    t.integer  "presenter_id",    limit: 4
    t.boolean  "is_open",                     default: false
    t.string   "lat",             limit: 255
    t.string   "lng",             limit: 255
    t.string   "secret",          limit: 255
    t.integer  "current_page",    limit: 4,   default: 0
    t.boolean  "deleted",                     default: false
    t.boolean  "live",                        default: false
    t.string   "address",         limit: 255
    t.boolean  "ready",                       default: false
    t.string   "zoom",            limit: 255
    t.string   "position_offset", limit: 255
  end

  add_index "sessions", ["key"], name: "index_sessions_on_key", unique: true, using: :btree
  add_index "sessions", ["presenter_id"], name: "index_sessions_on_presenter_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "username",   limit: 255
    t.string   "password",   limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "first_name", limit: 255
    t.string   "last_name",  limit: 255
    t.string   "email",      limit: 255
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree

  create_table "viewers", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.integer  "session_id", limit: 4
    t.integer  "user_id",    limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "viewers", ["session_id"], name: "index_viewers_on_session_id", using: :btree
  add_index "viewers", ["user_id"], name: "index_viewers_on_user_id", using: :btree

  add_foreign_key "annotations", "resources"
  add_foreign_key "draw_leases", "sessions"
  add_foreign_key "draw_leases", "viewers"
  add_foreign_key "feedbacks", "resources"
  add_foreign_key "feedbacks", "sessions"
  add_foreign_key "feedbacks", "viewers"
  add_foreign_key "notifications", "sessions"
  add_foreign_key "notifications", "viewers"
  add_foreign_key "presenters", "users"
  add_foreign_key "resources", "presenters"
  add_foreign_key "resources", "sessions"
  add_foreign_key "sessions", "presenters"
  add_foreign_key "viewers", "sessions"
  add_foreign_key "viewers", "users"
end
