require 'grape-swagger'

module PowerballApi
  module V1

    class Base < Grape::API
      prefix 'api'
      version 'v1', using: :path

      helpers do
        
        def permitted_params
          @permitted_params ||= declared(params, include_missing: false, include_parent_namespaces: false)
        end

        def authenticate!
          error!('Unauthorized. Invalid or expired token.', 401) unless current_user
        end

        def current_user
          # find token. Check if valid.
          token = ApiKey.where(access_token: params[:token]).first
          if token && !token.expired?
            @user = User.find(token.user_id)
          else
            false
          end
        end
      end

      mount Authenticate
      mount Signup

      namespace :secured do
        before do
          authenticate!
        end
        mount Presenters
        mount Sessions
        mount Viewers
        mount Feedbacks
        mount Resources
        mount Users
        mount Notifications
        mount Annotations
      end

      add_swagger_documentation api_version: 'v1',
                                hide_documentation_path: true
    end
  end

end
