class PowerballApi::V1::Annotations < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :annotations do

    params do
      requires :note, type: String, desc: "Notes about the resource"
      requires :resource_id, type: String, desc: "Resource id"
      requires :page, type: Integer, desc: "Annotation Page"
      requires :x_offset, type: String, desc: "X offset  image"
      requires :y_offset, type: String, desc: "Y offset image"
      requires :zoom, type: String, desc: "Zoom image"
      requires :draw, type: Rack::Multipart::UploadedFile, desc: "Base64 encoded data"
      optional :draw_token, type: String, desc: "Draw lease token"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

       presenters = Presenter.where("user_id = ?", @user.id)
       error!({status: 'error', message:'Presenter not found.'}, 404) if presenters.nil? 

       resource = Resource.find_by_id(params[:resource_id])
       error!({status: 'error', message:'Resource not found.'}, 404) if resource.nil?        

       if params[:draw_token].present?
        viewer = Viewer.find_by_user_id(@user.id)
        draw_token = DrawLease.where(draw_token: params[:draw_token], viewer: viewer).first
        unless draw_token
          error!({status: 'error', message:'Draw token not found.'}, 404)
        end
       else
        session = Session.find_by_key(resource.session.key)
        error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user
       end

       annotation = Annotation.find_by_resource_id_and_page(params[:resource_id], params[:page])
       if !annotation.nil?
        annotation.destroy
       end

    	 annotation = Annotation.new
    	 annotation.note = params[:note]
       annotation.page = params[:page]
       annotation.x_offset = params[:x_offset]
       annotation.y_offset = params[:y_offset]
       annotation.zoom = params[:zoom]
    	 annotation.resource = resource

       uploaded_data = ActionDispatch::Http::UploadedFile.new(params[:draw])
       dat = ::MiniMagick::Image.open uploaded_data.path
       annotation.draw = dat

    	 if annotation.save
        unless draw_token.nil?
          draw_token.destroy
        end
    	 	present annotation, with: PowerballApi::V1::Entities::AnnotationResponseEntity
    	 else
    	 	error!('Error while creating annotation.', 500)
    	 end
    end

    get ":id", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do   

       annotation = Annotation.find_by_id(params[:id])
       error!({status: 'error', message:'Annotation not found.'}, 404) if annotation.nil? 
       
       present annotation, with: PowerballApi::V1::Entities::AnnotationResponseEntity
    end

  end
end