module PowerballApi::V1::Entities
	class SessionResponseEntity < Grape::Entity

		expose :key
		expose :name
		expose :presenter, using: PresenterResponseEntity
		expose :viewers, using: ViewerResponseEntity
		expose :feedbacks, using: FeedbackResponseEntity
		expose :resources, using: ResourceResponseEntity
		expose :is_open
		expose :current_page
		expose :total_pages do |session, options|
			session.total_pages
		end
		expose :live
		expose :distance do |session, options|
			session.distance(options[:lat], options[:lng])
		end
		
	end
end