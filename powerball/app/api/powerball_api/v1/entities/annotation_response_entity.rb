module PowerballApi::V1::Entities
	class AnnotationResponseEntity < Grape::Entity

		expose :id
		expose :note
		expose :draw
		expose :page
		expose :x_offset
		expose :y_offset
		expose :zoom		
	end
end