module PowerballApi::V1::Entities
	class UserResponseEntity < Grape::Entity

		expose :username
		expose :id
		expose :first_name
		expose :last_name
		expose :email
		
	end
end