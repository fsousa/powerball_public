module PowerballApi::V1::Entities
	class FeedbackResponseEntity < Grape::Entity

		expose :description
		expose :viewer, using: ViewerResponseEntity
		expose :resource, using: ResourceResponseEntity
	end
end