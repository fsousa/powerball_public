module PowerballApi::V1::Entities
	class SessionShortResponseEntity < Grape::Entity

		expose :key
		expose :name
		expose :presenter, using: PresenterResponseEntity
		expose :is_open
		expose :number_of_viewers do |session, options|
			if session.viewers == nil
				 0
			else
				session.viewers.count
			end
		end
		expose :live
		expose :distance do |session, options|
			session.distance(options[:lat], options[:lng])
		end
	end
end