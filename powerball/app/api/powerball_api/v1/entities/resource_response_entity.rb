module PowerballApi::V1::Entities
	class ResourceResponseEntity < Grape::Entity

		expose :id
		expose :name
		expose :data
		expose :resource_type
		expose :position
		expose :annotations, using: AnnotationResponseEntity
		
	end
end