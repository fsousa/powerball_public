class PowerballApi::V1::Viewers < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :viewers do
  	
    get "", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do
    	 present Viewer.all, with: PowerballApi::V1::Entities::ViewerResponseEntity
    end


    params do
      requires :name, type: String, desc: "Viewers name"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do
    	 viewer = Viewer.new
    	 viewer.name = params[:name]
    	 viewer.user = @user

    	 if viewer.save 
    	 	{ status: "Created"}
    	 else
    	 	error!('Error while creating viewer.', 500)
    	 end
    end
  end
end