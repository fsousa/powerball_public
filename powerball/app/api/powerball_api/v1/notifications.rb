class PowerballApi::V1::Notifications < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :notifications do

    params do
      requires :key, type: String, desc: "Session key"
      requires :content, type: String, desc: "Notification content"
      requires :notification_type, type: String, desc: "Notification type"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

       viewer = Viewer.find_by_user_id(@user.id)
       error!({status: 'error', message:'Viewer not found.'}, 404) if viewer.nil? 

       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 

    	 notification = Notification.new
       notification.viewer = viewer
    	 notification.session = session
    	 notification.notification_content = params[:content]
       notification.notification_type = params[:notification_type]

    	 if notification.save 
    	 	{ status: "Created"}
    	 else
    	 	error!('Error while creating notification.', 500)
    	 end
    end
  end
end