class PowerballApi::V1::Signup < Grape::API
  resource :signup do
    desc "Creates customer and returns access_token if valid login"
    params do
      requires :username, type: String, desc: "Username"
      requires :password, type: String, desc: "Password"
      requires :first_name, type: String, desc: "First Name"
      requires :last_name, type: String, desc: "Last Name"
      requires :email, type: String, desc: "Email"
    end
    post :create do

      user = User.find_by_username(params[:username])
      error!({status: "error", message: "Username already exist"}, 422) unless user.nil?

      user = User.find_by_email(params[:email])
      error!({status: "error", message: "Email already exist"}, 422) unless user.nil?

      user = User.new
      user.username = params[:username]
      user.password = params[:password]
      user.first_name = params[:first_name]
      user.last_name = params[:last_name]
      user.email = params[:email]

      if user.save
        key = ApiKey.create(user_id: user.id)
        {token: key.access_token}
      else
        error!({status: "error", message: user.errors.full_messages[0]}, 422)
      end
    end
  end
end