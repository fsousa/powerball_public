class PowerballApi::V1::Authenticate < Grape::API
  resource :auth do
    desc "Creates and returns access_token if valid login"
    params do
      requires :username, type: String, desc: "Username or email address"
      requires :password, type: String, desc: "Password"
    end
    post :login do

      user = User.find_by_username(params[:username])

      if user && user.authenticate(params[:password])
        key = ApiKey.create(user_id: user.id)
        {token: key.access_token}
      else
        error!('Failed to authenticate customer.', 401)
      end
    end

    desc "Returns valid if logged in correctly"
    params do
      requires :token, type: String, desc: "Access token."
    end
    get :validate do
      authenticate!
      { status: "valid" }
    end
  end
end