class PowerballApi::V1::Presenters < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :presenters do
  	
    get "", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do
    	 present Presenter.all, with: PowerballApi::V1::Entities::PresenterResponseEntity
    end

    get ":id", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do
    	 presenter = Presenter.find_by_id(params[:id])
    	 error!({status: 'error', message:'Presenter not found.'}, 404) if presenter.nil? 
    	 
    	 present presenter, with: PowerballApi::V1::Entities::PresenterResponseEntity
    end


    params do
      requires :name, type: String, desc: "Presenters name"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do
    	 presenter = Presenter.new
    	 presenter.name = params[:name]
    	 presenter.user = @user

    	 if presenter.save 
    	 	{ status: "Created"}
    	 else
    	 	error!('Error while creating presenter.', 500)
    	 end
    end
  end
end