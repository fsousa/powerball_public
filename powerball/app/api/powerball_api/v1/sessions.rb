class PowerballApi::V1::Sessions < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :sessions do

    params do
      requires :lat, type: String, desc: "User's Latitude"
      requires :lng, type: String, desc: "User's Longitude"
    end
  	
    get "", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do
       presenter = Presenter.find_by_user_id(@user.id)

       if presenter.nil?
          result = Session.all
       else
          result = Session.where.not(:presenter_id => presenter.id)
       end

    	 present result, with: PowerballApi::V1::Entities::SessionShortResponseEntity, :lat => params[:lat], :lng => params[:lng]
    end

    get "presenter", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       presenter = Presenter.find_by_user_id(@user.id)

       if presenter.nil?
        []
       else
        present Session.where(:presenter_id => presenter.id), with: PowerballApi::V1::Entities::SessionShortResponseEntity
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      optional :secret, type: String, desc: "Sessions secret"
    end
    get "details", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Not Found'}, 404) if session.nil? 

       if session.presenter.user == @user || session.is_open || session.secret == params[:secret]
          present session, with: PowerballApi::V1::Entities::SessionResponseEntity
       else
        error!({status: 'error', message:'Forbidden'}, 401)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      requires :secret, type: String, desc: "Sessions secret"
    end
    get "checkSecret", http_codes: [
        [200, 'Ok'],
        [403, "Forbidden"],
        [500, "Internal Server Error"]
    ] do
       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Forbidden'}, 404) if session.nil? 

       if !session.is_open && session.secret == params[:secret]
          { status: "OK"}
       else
        error!({status: 'error', message:'Forbidden'}, 404)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      optional :secret, type: String, desc: "Sessions secret"
    end
    get "resources", http_codes: [
        [200, 'Ok'],
        [403, "Forbidden"],
        [500, "Internal Server Error"]
    ] do
       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Forbidden'}, 404) if session.nil? 

       if session.is_open || session.secret == params[:secret]
          present session.resources, with: PowerballApi::V1::Entities::ResourceResponseEntity
       else
        error!({status: 'error', message:'Forbidden'}, 404)
       end
    end


    params do
      requires :name, type: String, desc: "Sessions name"
      requires :is_open, type: String, desc: "Sessions privacy setting"
      requires :lat, type: String, desc: "Sessions Latitude"
      requires :lng, type: String, desc: "Sessions Longitude"
      optional :secret, type: String, desc: "Sessions password"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

    	 session = Session.new
    	 session.name = params[:name]
       session.key = SecureRandom.hex
       session.is_open = params[:is_open]
       session.lat = params[:lat]
       session.lng = params[:lng]
       session.live = true
       session.ready = false
       session.zoom = "1"
       session.position_offset = "0"

       
       presenters = Presenter.where("user_id = ?", @user.id)
       if presenters.empty? 
          presenter = Presenter.new
          presenter.name = @user.fullname
          presenter.user = @user
          presenter.save
          session.presenter = presenter
       else
          session.presenter = presenters.first
       end

       if params[:is_open] == "false"
          session.secret = params[:secret]
       end

    	 if session.save 
    	 	present session, with: PowerballApi::V1::Entities::SessionShortResponseEntity, :lat => params[:lat], :lng => params[:lng]
    	 else
    	 	error!('Error while creating session.', 500)
    	 end
    end

    params do
      requires :key, type: String, desc: "Sessions key"
    end
    delete "", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

       session = Session.unscoped.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
       error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user

       session.deleted = true
       session.live = false
       if session.save
         { status: "OK"}
       else
        error!('Error deleting session.', 500)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions key"
      requires :current_page, type: String, desc: "Sessions Current Page"
    end
    post "changePage", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

      session = Session.find_by_key(params[:key])
      error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
      error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user

      error!({status: 'error', message:'No more pages available.'}, 500) if params[:current_page] == session.total_pages - 1

      session.current_page = params[:current_page]
      if session.save
        { status: "OK"}
      else
        error!('Error while adding saving session.', 500)
      end

       
    end

    params do
      requires :key, type: String, desc: "Sessions key"
      requires :zoom, type: String, desc: "Sessions Current Zoom"
      requires :position_offset, type: String, desc: "Sessions Position Offset"
    end
    post "changeZoom", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

      session = Session.find_by_key(params[:key])
      error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
      error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user

      session.zoom = params[:zoom]
      session.position_offset = params[:position_offset]
      if session.save
        { status: "OK"}
      else
        error!('Error while adding saving session.', 500)
      end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      requires :user_id, type: String, desc: "Viewer id"
      requires :granted, type: Boolean, desc: "Token granted"
    end
    post "generateSessionDrawLease", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

       session = Session.unscoped.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
       error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user

       viewer = Viewer.where("user_id = ?", params[:user_id]).first
       error!({status: 'error', message:'Viewer not found.'}, 404) if viewer.nil? 

       lease = DrawLease.new
       lease.viewer = viewer
       lease.session = session
       if params[:granted] == true 
        lease.draw_token = SecureRandom.hex
       else
        lease.draw_token = "error"
       end

       if lease.save 
        { draw_token: lease.draw_token}
       else
        error!('Error while creating draw lease token.', 500)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      optional :secret, type: String, desc: "Sessions password"
    end
    put "addViewToSession", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
       error!({status: 'error', message:'Secret do not match.'}, 403) if !session.is_open && session.secret != params[:secret] 

       viewers = Viewer.where("user_id = ?", @user.id)
       if viewers.empty? 
        viewer = Viewer.new
        viewer.name = @user.fullname
        viewer.user = @user
        viewer.session = session
       else
        viewer = viewers.first
        viewer.session = session
       end

       if viewer.save 
        present viewer, with: PowerballApi::V1::Entities::ViewerResponseEntity
       else
        error!('Error while adding viewer to session.', 500)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
    end
    put "removeViewFromSession", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 

       viewers = Viewer.where("user_id = ? AND session_id = ?", @user.id, session.id)
       if viewers.destroy_all 
        { status: "OK"}
       else
        error!('Error while adding viewer to session.', 500)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      requires :live, type: Boolean, desc: "Presenter's Sessions Live"
    end
    put "setPresenterLiveOnSession", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
       error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user

       session.live = params[:live]

       if session.save 
        { status: "OK"}
       else
        error!('Error while set presenter live on session.', 500)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
    end
    put "setSessionReady", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       session = Session.unscoped.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 
       error!({status: 'error', message:'Owner do not match.'}, 403) if session.presenter.user != @user

       session.ready = true

       if session.save 
        { status: "OK"}
       else
        error!('Error while set presenter live on session.', 500)
       end
    end

    params do
      requires :key, type: String, desc: "Sessions Key"
      requires :resource_id, type: String, desc: "Resource id"
    end
    put "addResourceToSession", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       session = Session.unscoped.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? || session.presenter.user != @user

       resource = Resource.find_by_id(params[:resource_id])
       error!({status: 'error', message:'Resource not found.'}, 404) if resource.nil? 
       
       session.resources << resource

       if session.save 
        { status: "OK"}
       else
        error!('Error while adding resource to session.', 500)
       end
    end
  end
end