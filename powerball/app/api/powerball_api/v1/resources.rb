class PowerballApi::V1::Resources < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :resources do
  	
    get "", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do
    	 present Resource.all, with: PowerballApi::V1::Entities::ResourceResponseEntity
    end

    params do
      requires :resource_id, type: String, desc: "Resources id"
      requires :picture, type: Rack::Multipart::UploadedFile, desc: "Base64 encoded image"
    end
    post 'uploadPicture', http_codes: [
      [201, "Created"],
      [401, "Unauthorized"],
      [500, "Internal Server Error"]
    ] do

      resource = Resource.find_by_id(params[:resource_id])
      error!({status: 'error', message:'Resource not found.'}, 404) if resource.nil? 

      uploaded_image = ActionDispatch::Http::UploadedFile.new(params[:picture])

      img = ::MiniMagick::Image.open uploaded_image.path

      resource.picture = img
      if resource.save
        { status: "OK",
          picture: resource.picture.url}
      else
        error!("Internal Server Error", 500)
      end
    end


    params do
      requires :name, type: String, desc: "Resources name"
      requires :key, type: String, desc: "Session Key"
      requires :position, type: Integer, desc: "Resources position"
      requires :resource_type, type: String, desc: "Resource mimo type"
      requires :resource_data, type: Rack::Multipart::UploadedFile, desc: "Base64 encoded data"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do


       presenters = Presenter.where("user_id = ?", @user.id)
       error!({status: 'error', message:'Presenter not found.'}, 404) if presenters.nil? 

       session = Session.unscoped.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil?

    	 resource = Resource.new
    	 resource.name = params[:name]
    	 resource.presenter = presenters.first
       resource.session = session
       resource.position = params[:position]
       resource.resource_type = params[:resource_type]

       uploaded_data = ActionDispatch::Http::UploadedFile.new(params[:resource_data])
       dat = ::MiniMagick::Image.open uploaded_data.path
       resource.data = dat

    	 if resource.save 
    	 	present resource, with: PowerballApi::V1::Entities::ResourceResponseEntity
    	 else
    	 	error!('Error while creating resource.', 500)
    	 end
    end
  end
end