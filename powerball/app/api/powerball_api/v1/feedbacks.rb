class PowerballApi::V1::Feedbacks < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :feedbacks do

    params do
      requires :key, type: String, desc: "Session key"
    end
    get "session", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

      session = Session.find_by_key(params[:key])
      error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 

      present Feedback.where("session_id = ?", session.id), with: PowerballApi::V1::Entities::FeedbackResponseEntity
    end


    params do
      requires :key, type: String, desc: "Session key"
      requires :resource_id, type: String, desc: "Resource id"
      requires :description, type: String, desc: "Description text"
  	end
    post "create", http_codes: [
        [201, 'Created'],
        [500, "Internal Server Error"]
    ] do

       viewer = Viewer.find_by_user_id(@user.id)
       error!({status: 'error', message:'Viewer not found.'}, 404) if viewer.nil? 

       session = Session.find_by_key(params[:key])
       error!({status: 'error', message:'Session not found.'}, 404) if session.nil? 

       resource = Resource.find_by_id(params[:resource_id])
       error!({status: 'error', message:'Resource not found.'}, 404) if resource.nil? 

    	 feedback = Feedback.new
       feedback.viewer = viewer
    	 feedback.session = session
    	 feedback.resource = resource
       feedback.description = params[:description]

    	 if feedback.save 
    	 	{ status: "Created"}
    	 else
    	 	error!('Error while creating feedback.', 500)
    	 end
    end
  end
end