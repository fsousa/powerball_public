class PowerballApi::V1::Users < Grape::API

  params do
      requires :token, type: String, desc: "Token"
  end
  resource :user do
    desc "Get user's profile"
    get "", http_codes: [
        [200, 'Ok'],
        [500, "Internal Server Error"]
    ] do

       present @user, with: PowerballApi::V1::Entities::UserResponseEntity
    end
  end
end