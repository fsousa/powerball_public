class User < ActiveRecord::Base

	has_many :api_keys, :dependent => :delete_all

	validates_presence_of :username
  	validates_presence_of :first_name
  	validates_presence_of :last_name

  	validates :email, email: true

	def authenticate(password) 
		self.password == password
	end

	def fullname
	"#{self.first_name} #{self.last_name}"
	end
	
end
