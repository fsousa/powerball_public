class Presenter < ActiveRecord::Base
	has_many :sessions
	has_many :resources
	
	belongs_to :user

end
