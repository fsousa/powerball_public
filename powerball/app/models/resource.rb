class Resource < ActiveRecord::Base
	belongs_to :session
	belongs_to :presenter

	has_many :annotations, :dependent => :delete_all

	mount_uploader :data, DataUploader

	default_scope { order(position: :asc) }
end
