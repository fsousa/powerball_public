class Annotation < ActiveRecord::Base
  belongs_to :resource

  mount_uploader :draw, DataUploader

  after_save :notify_viewers

  def notify_viewers
  		draw = "{\"id\": \"#{self.id}\", \"session_key\": \"#{self.resource.session.key}\", \"url\": \"#{self.draw.url}\", \"x_offset\": \"#{self.x_offset}\", \"y_offset\": \"#{self.y_offset}\", \"zoom\": \"#{self.zoom}\"}"
  		WebsocketRails[:draw].trigger(self.resource.session.key, draw)
  end 


end
