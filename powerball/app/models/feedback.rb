class Feedback < ActiveRecord::Base
  belongs_to :viewer
  belongs_to :session
  belongs_to :resource
end
