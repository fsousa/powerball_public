class Notification < ActiveRecord::Base
  belongs_to :session
  belongs_to :viewer


  after_save :notify_presenter

  def notify_presenter
  	notification = "{\"notification_content\": \"#{self.notification_content}\", \"notification_type\": \"#{self.notification_type}\"}"
  	WebsocketRails[:notification].trigger(self.session.presenter.user.username, notification)
  end 

end
