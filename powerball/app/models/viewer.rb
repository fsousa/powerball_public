class Viewer < ActiveRecord::Base
  belongs_to :session
  belongs_to :user

  has_many :notifications, :dependent => :delete_all
  has_many :feedbacks, :dependent => :delete_all
  has_many :draw_leases, :dependent => :delete_all
end
