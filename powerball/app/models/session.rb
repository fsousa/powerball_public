class Session < ActiveRecord::Base
	belongs_to :presenter
	has_many :viewers
	has_many :resources
	has_many :feedbacks
	has_many :notifications

	after_save :notify_viewers

	default_scope { where(deleted: false, ready: true)}

	reverse_geocoded_by :lat, :lng
	after_validation :reverse_geocode

	def total_pages
		self.resources.count
	end

	def notify_viewers
  		task = {"current_page": self.current_page, "session_key": self.key, "zoom": self.zoom, "position_offset": self.position_offset}
  		WebsocketRails[:task].trigger(self.key, task)
  	end 

  	def distance(lat, lng)
  		return 99999999 if self.lat.blank? || self.lng.blank?
    	return 99999999 if lat.blank? || lng.blank?
  		Geocoder::Calculations.distance_between([self.lat.to_f,self.lng.to_f], [lat.to_f,lng.to_f], :units => :km)
  	end
end
