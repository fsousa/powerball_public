class DrawLease < ActiveRecord::Base
  belongs_to :session
  belongs_to :viewer

  after_save :notify_viewer

  def notify_viewer 
  		draw_token = "{\"draw_token\": \"#{self.draw_token}\"}"
  		WebsocketRails[:notifyviewer].trigger(self.viewer.user.id.to_s, draw_token)

  		if self.draw_token == "error"
  			self.destroy
  		end
  	end 
  
end
