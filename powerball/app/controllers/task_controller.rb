class TaskController < WebsocketRails::BaseController
  
  def create
    # The `message` method contains the data received
    task = Task.new message
    if !task.nil?
      broadcast_message :task, task
    end
  end

end