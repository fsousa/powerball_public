class NotificationViewerController < WebsocketRails::BaseController
  
  def notify
    # The `message` method contains the data received
    broadcast_message :notifyviewer, message

    
  end

end