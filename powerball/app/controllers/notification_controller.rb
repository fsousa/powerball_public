class NotificationController < WebsocketRails::BaseController
  
  def create
    # The `message` method contains the data received
    broadcast_message :notification, message

    
  end

end